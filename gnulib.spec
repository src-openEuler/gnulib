%global commit bad1faa7f5fd21a315c45cc6327cfc9874f499fb
%global githead %(printf %%.7s %commit)
%global gitdate 20230709
Name:             gnulib
Version:          0
Release:          48.%{gitdate}git
Summary:          The GNU Portability Library
License:          Public Domain and BSD-3-Clause and GPL-2.0-or-later and GPL-3.0-only and GPL-3.0-or-later and LGPL-2.0-only and LGPL-2.1-or-later and LGPL-3.0-or-later
URL:              https://www.gnu.org/software/gnulib
Source0:          https://git.savannah.gnu.org/gitweb/?p=gnulib.git;a=snapshot;h=%{githead};sf=tgz;name=gnulib-%{githead}.tar.gz#/gnulib-%{githead}.tar.gz
Source1:          https://erislabs.net/gitweb/?p=gnulib.git;a=blob_plain;hb=HEAD;f=debian/manpages/check-module.1
Source2:          https://erislabs.net/gitweb/?p=gnulib.git;a=blob_plain;hb=HEAD;f=debian/manpages/gnulib-tool.1

Patch0:           test-u8-strstr-alarm.diff
BuildRequires:    perl-generators texinfo gettext-devel bison gperf libtool help2man git make ncurses-devel
BuildRequires:    gcc_secure java-11-openjdk-devel

%description
Gnulib is a central location for common GNU code, intended to be
shared among GNU packages. It can be used to improve portability
and other functionality in your programs.

%package devel
Summary:          Provides devel files for gnulib
BuildArch:        noarch
Provides:         gnulib
Requires:         gettext-devel bison coreutils gperf libtool make texinfo diffutils patch

%description devel
This package contains development files for gnulib.

%package help
Summary:          Provides documentation for gnulib modules
License:          GFDL-1.1-or-later and GPL-2.0-or-later
Requires(post):   info
Requires(preun):  info
Requires:         gnulib-devel = %{version}-%{release}
Provides:         gnulib-docs = %{version}-%{release}
Obsoletes:        gnulib-docs < %{version}-%{release}
Provides:         git-merge-changelog = %{version}-%{release}
Obsoletes:        git-merge-changelog < %{version}-%{release}

%description help
This package contains help docs for gnulib. And Provides ChangeLog files.

%prep
%autosetup -n gnulib-%{githead} -p1 -Sgit

toRemove="lib-symbol-visibility havelib .*-obsolete localcharset gettext-h gettext alloca-opt alloca "

list="$(./gnulib-tool --list)"
for item in $toRemove
do
   list="$(echo $list| sed "s:\b$item\b::g")"
done
./gnulib-tool --create-testdir --with-tests --with-obsolete --avoid=alloca --avoid=lib-symbol-visibility --avoid=havelib --dir=build-tests $list
rm lib/javaversion.class
./gnulib-tool --create-testdir --dir=build-gnulib-root git-merge-changelog

cp lib/timevar.def build-tests/gllib/

%build
cd build-gnulib-root
%configure --prefix=%_prefix
%make_build
cd -

cd build-tests
%configure --prefix=%_prefix
%make_build
cd -

home_path=`ls /usr/lib/jvm | grep java-11-openjdk`
[ -z "$JAVA_HOME" ] && export JAVA_HOME=%{_jvmdir}/${home_path}
javac -d lib -source 11 -target 11 lib/javaversion.java
%make_build MODULES.html
sed -i -r 's#HREF="(lib|m4|modules)#HREF="%{_datadir}/gnulib/\1#g' MODULES.html
sed -i "/^[ ]*gnulib_dir=/s#\`[^\`]*\`#%{_datadir}/gnulib#" gnulib-tool
%make_build info html

rm -rf */.cvsignore */.gitignore */.gitattributes lib/.cppi-disable lib/uniname/gen-uninames.lisp

%check
make -C build-tests check VERBOSE=1

%install
install -d %{buildroot}%{_datadir}/gnulib
install -d %{buildroot}%{_bindir}
install -d %{buildroot}%{_datadir}/info
install -d %{buildroot}%{_docdir}/gnulib
install -d %{buildroot}%{_mandir}/man1

cp -p check-module %{buildroot}%{_bindir}
cp -p gnulib-tool %{buildroot}%{_bindir}
cp -rp build-aux lib m4 modules config tests %{buildroot}%{_datadir}/gnulib/
install -d %{buildroot}%{_datadir}/gnulib/doc
cp -arv doc/relocatable.texi %{buildroot}%{_datadir}/gnulib/doc

cp -p doc/gnulib.info %{buildroot}%{_datadir}/info/
cp -p doc/gnulib.html MODULES.html NEWS COPYING ChangeLog users.txt doc/COPYING* %{buildroot}%{_docdir}/gnulib/

%make_install -C build-gnulib-root
help2man -N --no-discard-stderr %{buildroot}%{_bindir}/git-merge-changelog | gzip -9c > %{buildroot}%{_mandir}/man1/git-merge-changelog.1.gz

%post help
/sbin/install-info %{_infodir}/gnulib.info %{_infodir}/dir || :
%preun help
if [ $1 = 0 ] ; then
  /sbin/install-info --delete %{_infodir}/gnulib.info %{_infodir}/dir || :
fi

%files devel
%{_datadir}/gnulib/
%{_bindir}/gnulib-tool
%{_bindir}/check-module
%{_docdir}/gnulib/
%exclude %{_docdir}/gnulib/MODULES.html
%exclude %{_docdir}/gnulib/gnulib.html

%files help
%{_datadir}/info/gnulib.info.gz
%{_docdir}/gnulib/gnulib.html
%{_docdir}/gnulib/MODULES.html
%{_mandir}/*/git-merge-changelog.*
%{_bindir}/git-merge-changelog
%license doc/COPYINGv2

%changelog
* Thu Jul 18 2024 yaoxin <yao_xin001@hoperun.com> - 0-48.20230709git
- License compliance rectification

* Fri Aug 25 2023 wangkai <13474090681@163.com> - 0-47.20230709git
- Upgrade to version 47.20230709git

* Tue Jun 21 2022 Ge Wang <wangge20@h-partners.com> - 0-43.20220212git
- Upgrade to version 43.20220212git

* Wed Feb 23 2022 yaoxin <yaoxin30@huawei.com> - 0-29.20180720git
- Fix CVE-2018-17942

* Sun Mar 21 2021 lingsheng <lingsheng@huawei.com> - 0-28.20180720git
- Add gcc_secure to fix test_localeconv fail

* Wed Jan  8 2020 sunguoshuai <sunguoshuai@huawei.com> - 0-27.20180720git
- Delete unwanted files.

* Tue Dec 31 2019 sunguoshuai <sunguoshuai@huawei.com> - 0-27.20180720git
- Init package.
